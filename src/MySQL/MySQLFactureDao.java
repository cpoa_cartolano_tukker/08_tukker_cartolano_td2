package MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import dao.Connexion;
import dao.InterfaceGenericFactureDao;
import metiers.Facture;


public class MySQLFactureDao implements InterfaceGenericFactureDao {

	private static MySQLFactureDao instance;

	public static MySQLFactureDao getInstance() {

		if (instance == null) {
			instance = new MySQLFactureDao();
		}

		return instance;
	}

	@Override
	public Facture getById(int id) {
		Facture produit=null;
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM facture WHERE idfac=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				produit = new Facture(res.getInt("idfacture"),res.getInt("idclient"),res.getString("datefacture")); 
					
				res.close();
			} else {
				System.out.println("Facture inexistante !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return produit;
	}

	@Override
	public void create(Facture objet) {
		int id;
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
					"INSERT INTO tva (idfacture,idclient, datefacture) VALUES (?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			requete.setInt(1, objet.getIdfacture());
			requete.setInt(2, objet.getIdclient());
			requete.setString(3, objet.getDatefacture());
			
			requete.executeUpdate();

			ResultSet res = requete.getGeneratedKeys();
			if (res.next()) {
				id = res.getInt(1);
				res.close();
			}

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}


	}

	@Override
	public void update(Facture objet) {
		
		try {
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
				"UPDATE produit SET idclient =? , datefacture=? WHERE idfacture=?");
		requete.setInt(1, objet.getIdclient());
		requete.setString(2, objet.getDatefacture());
		requete.setInt(3, objet.getIdfacture());
		requete.executeUpdate();

		if (requete != null) {
			requete.close();
		}
	} catch (SQLException se) {
		System.err.println("Pb SQL :" + se.getMessage());}
	}

	

	@Override
	public void delete(Facture objet) {
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("DELETE FROM facture WHERE idfacture=?");
			requete.setInt(1, objet.getIdfacture());
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}

	

	@Override
	public List<Facture> findAllFacture() {
		List<Facture> listfacture = new ArrayList<Facture>();
		try{
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM Facture ");
		
		ResultSet res = requete.executeQuery();

		while (res.next()) {
			
			
			listfacture.add(new Facture(res.getInt("idfacture"),res.getInt("idclient"),res.getString("datefacture")));
				
			
			
		} 
		
		
		

		if (requete != null) {
			requete.close();
		}

	} catch (SQLException se) {
		System.out.println("Pb SQL " + se.getMessage());
	}

	return listfacture;
	}

	


}