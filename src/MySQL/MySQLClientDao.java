package MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import dao.Connexion;
import dao.InterfaceGenericClientDao;
import metiers.Client;


public class MySQLClientDao implements InterfaceGenericClientDao {

	private static MySQLClientDao instance;

	public static MySQLClientDao getInstance() {

		if (instance == null) {
			instance = new MySQLClientDao();
		}

		return instance;
	}

	@Override
	public Client getById(int id) {
		Client produit=null;
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM client WHERE idclient=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				produit = new Client(res.getInt("idclient"),res.getString("nomclient"),res.getString("adclient")); 
					
				res.close();
			} else {
				System.out.println("Client inexistant !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return produit;
	}

	@Override
	public void create(Client objet) {
		int id;
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
					"INSERT INTO tva (idclient,nomclient, adclient) VALUES (?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			requete.setInt(1, objet.getIdclient());
			requete.setString(2, objet.getNomclient());
			requete.setString(3, objet.getAdclient());
			
			requete.executeUpdate();

			ResultSet res = requete.getGeneratedKeys();
			if (res.next()) {
				id = res.getInt(1);
				res.close();
			}

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}


	}

	@Override
	public void update(Client objet) {
		
		try {
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement(
				"UPDATE produit SET nomclient =? , adclient=? WHERE idclient=?");
		requete.setString(1, objet.getNomclient());
		requete.setString(2, objet.getAdclient());
		requete.setInt(3, objet.getIdclient());
		requete.executeUpdate();

		if (requete != null) {
			requete.close();
		}
	} catch (SQLException se) {
		System.err.println("Pb SQL :" + se.getMessage());}
	}

	

	@Override
	public void delete(Client objet) {
		try {
			PreparedStatement requete = Connexion.creerConnexion().prepareStatement("DELETE FROM facture WHERE idclient=?");
			requete.setInt(1, objet.getIdclient());
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}

	

	@Override
	public List<Client> findAllClient() {
		List<Client> listfacture = new ArrayList<Client>();
		try{
		PreparedStatement requete = Connexion.creerConnexion().prepareStatement("SELECT * FROM Client ");
		
		ResultSet res = requete.executeQuery();

		while (res.next()) {
			
			
			listfacture.add(new Client(res.getInt("idclient"),res.getString("nomclient"),res.getString("adclient")));
				
			
			
		} 
		
		
		

		if (requete != null) {
			requete.close();
		}

	} catch (SQLException se) {
		System.out.println("Pb SQL " + se.getMessage());
	}

	return listfacture;
	}

	


}