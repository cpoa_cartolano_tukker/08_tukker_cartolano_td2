package tests;

import static org.junit.Assert.*;
import metiers.Facture;

import org.junit.Test;

public class TestFacture {

	
	// Tests Constructeur
	
	@Test
	public void testConstructErreur() {
		
		
		try{
		Facture facture1 = new Facture(-11,2,"12/09/2009");
		fail("erreur l'id est inferieur a 0");
		}
		catch(IllegalArgumentException e){};
	}
	
	
	@Test
	public void testConstructReussi() {
		
		
		try{
			Facture facture1 = new Facture(11,2,"12/09/2009");
		
		}
		catch(IllegalArgumentException e){fail("erreur l'id est inferieur a 0");};
	}
	
	
	// Test get et set de l'id facture
	
	@Test
	public void testGetIdfacture(){
		Facture facture1 = new Facture(11,2,"12/09/2009");
		assertEquals(11,facture1.getIdfacture());
	}
	
	@Test
	public void testSetIdfactureErreur(){
		try{
			Facture facture1 = new Facture(11,2,"12/09/2009");
			facture1.setIdfacture(-11);
			fail("erreur id facture inferieur ou egal a 0");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetIdfactureReussi(){
		try{
			Facture facture1 = new Facture(11,2,"12/09/2009");
			facture1.setIdfacture(22);
			
		} catch(IllegalArgumentException e){
			fail("erreur id facture inferieur ou egal a 0");}
	}
	
	
	// Test get et set Idclient
	
	@Test
	public void testGetDate(){
		Facture facture1 = new Facture(11,2,"12/09/2009");
		assertEquals("12/09/2009",facture1.getDatefacture());
	}
	
	@Test
	public void testSetDateErreur(){
		try{
			Facture facture1 = new Facture(11,2,"12/09/2009");
			facture1.setDatefacture(null);
			fail("erreur date facture null");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetDateReussi(){
		try{
			Facture facture1 = new Facture(11,2,"12/09/2009");
			facture1.setDatefacture("14/09/2009");
			
		} catch(IllegalArgumentException e){
			fail("erreur date facture null");}
	}
	
	// Test get et set Idclient
	
	@Test
	public void testGetIdclient(){
		Facture facture1 = new Facture(11,2,"12/09/2009");
		assertEquals(2,facture1.getIdclient());
	}
	
	@Test
	public void testSetIdclientErreur(){
		try{
			Facture facture1 = new Facture(11,2,"12/09/2009");
			facture1.setIdclient(-1);
			fail("l'Id est inferieur ou egal a 0");
		} catch(IllegalArgumentException e){}
	}
	
	@Test
	public void testSetIdclientReussi(){
		try{
			Facture facture1 = new Facture(11,2,"12/09/2009");
			facture1.setIdclient(3);
			
		} catch(IllegalArgumentException e){
			fail("l'Id est inferieur ou egal a 0");}
	}


}