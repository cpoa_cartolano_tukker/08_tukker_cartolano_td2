package tests;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import dao.DAOFactory;
import metiers.Produit;

public class TestMySQLProduit {
	
	@Test
	
	public void test() {
		int choix; 
		do{
		DAOFactory daoProd = DAOFactory.getDAOFactory(dao.EPersistance.MYSQL);
		//MYSQL
		System.out.println("");
		System.out.println("Interface de modification de la table Produit");
		System.out.println("----------------------------------");
		System.out.println("Quelle action choisissez vous ?");
		System.out.println("1-Ajouter un produit");
		System.out.println("2-Modifier un produit");
		System.out.println("3-Supprimer un produit");
		System.out.println("4-Afficher la table produit");
		System.out.println("");
		System.out.println("0-Quitter");
		
		
		Scanner clavier = new Scanner(System.in);
		choix = clavier.nextInt();
		
			switch(choix){
				case 1:
					System.out.println("Vous voulez ajouter un produit");
					System.out.println("Nom du produit a ajouter ? ");
					String nom = clavier.next();
					System.out.println("Type de produit ?");
					int type = clavier.nextInt();
					System.out.println("Prix de ce produit ? ");
					double prix = clavier.nextDouble();
					
					daoProd.getProduitDAO().create(new Produit(1,nom,prix,type));	
					System.out.println("Le produit a été ajouté a la table ");
					System.out.println("");
					
					
					break;
					
				case 2:
					System.out.println("Vous voulez modifier un produit");
					System.out.println("Reference du produit a modifier ?");
					int id = clavier.nextInt();
					Produit produit = daoProd.getProduitDAO().getById(id);
					if (produit != null){
						System.out.println("produit a modifier : " + produit);
						
						System.out.println("nouveau nom du produit ?");
						nom = clavier.next();
						System.out.println("nouveau type de produit ?");
						type = clavier.nextInt();
						System.out.println("nouveau prix de ce produit ? ");
						prix = clavier.nextDouble();
						daoProd.getProduitDAO().update(new Produit(id,nom,prix,type));
					}
					
					break;
					
				case 3: 
					System.out.println("vous voulez supprimer un produit");
					System.out.print("Numéro du produit a supprimer ?");
					id = clavier.nextInt();
					
					produit = daoProd.getProduitDAO().getById(id);
					if ( produit !=null){
						daoProd.getProduitDAO().delete(new Produit(id,"t",2,2));
						System.out.println("Ce produit a été supprimé");
					}
					
					break;
					
				case 4:
					System.out.println("Contenu de la table :");
					List<Produit> listprod;
					listprod = daoProd.getProduitDAO().findAllProduit();
					for(Produit elem : listprod){
						System.out.println(elem);
					}
			}	
			
		
		} while(choix > 0);
	
	}
}
