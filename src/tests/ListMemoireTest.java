package tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import dao.DAOFactory;
import dao.EPersistance;
import dao.InterfaceGenericProduitDao;
import dao.InterfaceGenericTvaDao;
import metiers.Produit;
import metiers.Tva;

public class ListMemoireTest {
	
	private InterfaceGenericProduitDao dao;
	
	@Before
	public void setUp(){
		
		this.dao = DAOFactory.getDAOFactory(EPersistance.MEMORY).getProduitDAO();
	}

	@Test
	public void testAddProduit(){
		
		
		List<Produit> listProduit = dao.findAllProduit();
		
		for (Produit produit : listProduit){
			System.out.println("produit" + produit.toString());
		}
		
		dao.create(new Produit(1,"Tabouret",12,23));
		
		System.out.println("ajout");
		
		for (Produit produit : listProduit){
			System.out.println("produit" + produit.toString());
		}
	}
	
}
