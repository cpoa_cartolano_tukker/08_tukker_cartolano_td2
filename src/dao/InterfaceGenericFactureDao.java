

package dao;

import java.util.List;
import metiers.Facture;

public interface InterfaceGenericFactureDao extends IDao<Facture> {
        List<Facture> findAllFacture();

		
}