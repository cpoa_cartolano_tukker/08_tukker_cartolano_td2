package dao;

import java.util.List;

import metiers.TypeProduit;

public interface InterfaceGenericTypeProduitDao extends IDao<TypeProduit>{

	List<TypeProduit> findAllTypeProduit(); 
	
}
