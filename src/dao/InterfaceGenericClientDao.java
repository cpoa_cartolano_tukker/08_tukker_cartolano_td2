package dao;

import java.util.List;

import metiers.Client;

public interface InterfaceGenericClientDao extends IDao<Client>{

	List<Client> findAllClient(); 
	
}
