package metiers;

public class Tva {
	
	private String lib;
	private double taux;
	
	public Tva(String lib, double taux) {
		super();
		this.lib = lib;
		this.taux = taux;
		
		if (taux<=0)
		{
			throw new IllegalArgumentException("erreur le taux est inférieur à 0");
		}
		
		if (lib==null)
		{
			throw new IllegalArgumentException("erreur le libelle est vide");
		}
	}

	public String getLib() {
		return lib;
	}

	public void setLib(String lib) {
		
		if (lib==null)
		{
			throw new IllegalArgumentException("erreur le libelle est vide");
		}
		
		this.lib = lib;
	}

	public double getTaux() {
		return taux;
	}

	public void setTaux(double taux) {
		
		if (taux<=0)
		{
			throw new IllegalArgumentException("erreur le taux est inférieur à 0");
		}
		
		this.taux = taux;
	}

	@Override
	public String toString() {
		return "Tva [lib=" + lib + ", taux=" + taux + "]";
	}
	
	

}
