package metiers;

public class Produit {
	
	private int ref;
	private String nom;
	private double prixu;
	private int TypeProduit;
	
	public Produit(int ref, String nom, double prixu, int TypeProduit) {
		
		this.ref = ref;
		this.nom = nom;
		this.prixu = prixu;
		this.TypeProduit = TypeProduit;
		
		if (ref<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
		
		if (nom==null)
		{
			throw new IllegalArgumentException("erreur le nom est vide");
		}
		
		if (prixu<=0)
		{
			throw new IllegalArgumentException("erreur le prix unitaire est inférieur à 0");
		}
		
		if (TypeProduit<= 0)
		{
			throw new IllegalArgumentException("erreur le libelle est vide");
		}
	}

	public int getRef() {
		return ref;
	}

	public void setRef(int ref) {
		if (ref<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
		
		this.ref = ref;
	}

	public String getNom() {
		
		return nom;
	}

	public void setNom(String nom) {
		if (nom==null)
		{
			throw new IllegalArgumentException("erreur le nom est vide");
		}
		this.nom = nom;
	}

	public double getPrixu() {
		return prixu;
	}

	public void setPrixu(double prixu) {
		if (prixu<=0)
		{
			throw new IllegalArgumentException("erreur le prix unitaire est inférieur à 0");
		}
		this.prixu = prixu;
	}

	public int getTypeProduit() {
		return TypeProduit;
	}

	public void setTypeProduit(int TypeProduit) {
		if (TypeProduit<= 0) 
		{
			throw new IllegalArgumentException("erreur le libelle est vide");
		}
		
		this.TypeProduit = TypeProduit;
	}

	@Override
	public String toString() {
		return "ref=" + ref + " / nom = " + nom + " / prixu=" + prixu + " / TypeProduit=" + TypeProduit ;
	}

	public String AfficherRef(){
		return " ref = " + ref;
	}
	
	
	
	
}
