package metiers;

public class Client {
	
	private int idclient;
	private String nomclient;
	private String adclient;
	
	public Client(int idclient, String nomclient, String adclient) {
		super();
		this.idclient = idclient;
		this.nomclient = nomclient;
		this.adclient = adclient;
		
		if (idclient<=0)
		{
			throw new IllegalArgumentException("erreur idclient inférieur à 0");
		}
		
		if (nomclient==null)
		{
			throw new IllegalArgumentException("erreur nomclient vide");
		}
		
		if (adclient==null)
		{
			throw new IllegalArgumentException("erreur adclient vide");
		}
	}

	public int getIdclient() {
		return idclient;
	}

	public void setIdclient(int idclient) {
		
		
		if (idclient<=0)
		{
			throw new IllegalArgumentException("erreur idclient inférieur à 0");
		}
		this.idclient = idclient;
	}

	public String getNomclient() {
		
		return nomclient;
	}

	public void setNomclient(String nomclient) {
		if (nomclient==null)
		{
			throw new IllegalArgumentException("erreur idclient inférieur à 0");
		}
		this.nomclient = nomclient;
	}

	public String getAdclient() {
		return adclient;
	}

	public void setAdclient(String adclient) {
		if (adclient==null)
		{
			throw new IllegalArgumentException("erreur idclient inférieur à 0");
		}
		this.adclient = adclient;
	}

	@Override
	public String toString() {
		return "Client [idclient=" + idclient + ", nomclient=" + nomclient + ", adclient=" + adclient + "]";
	}
	
	

}
