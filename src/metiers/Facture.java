package metiers;



public class Facture {
	
	private int idclient;
	private int idfacture;
	private String datefacture;
	
	
	public Facture(int idfacture, int idclient, String datefacture ) {
		super();
		this.idfacture = idfacture;
		this.datefacture = datefacture;
		this.idclient = idclient;
		
		if (idfacture<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
		
		if (datefacture==null)
		{
			throw new IllegalArgumentException("erreur le nom est vide");
		}
		if (idclient<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
	
		
	}

	public int getIdfacture() {
		return idfacture;
	}

	public void setIdfacture(int idfacture) {
		this.idfacture = idfacture;
		if (idfacture<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
		
	}

	public String getDatefacture() {
		return datefacture;
	}

	public void setDatefacture(String datefacture) {
		this.datefacture = datefacture;
		if (datefacture==null)
		{
			throw new IllegalArgumentException("erreur le nom est vide");
		}
	}

	
	
	public int getIdclient() {
		return idclient;
	}

	public void setIdclient(int idclient) {
		this.idclient = idclient;
		if (idclient<=0)
		{
			throw new IllegalArgumentException("erreur la ref est inférieur à 0");
		}
	}

	@Override
	public String toString() {
		return "Facture [idclient=" + idclient + ", idfacture=" + idfacture + ", datefacture=" + datefacture + "]";
	}

	
	

}
