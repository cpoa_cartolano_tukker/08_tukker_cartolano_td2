package metiers;

public class TypeProduit {
	
	private int idtype;
	private String type;
	
	public TypeProduit(int idtype, String type) {
		super();
		this.idtype = idtype;
		this.type = type;
		
		if (idtype<=0)
		{
			throw new IllegalArgumentException("erreur l'id type inférieur à 0");
		}
		
		if (type==null)
		{
			throw new IllegalArgumentException("erreur le type est vide");
		}
	}

	public int getIdtype() {
		return idtype;
	}

	public void setIdtype(int idtype) {
		this.idtype = idtype;
		
		if (idtype<=0)
		{
			throw new IllegalArgumentException("erreur l'id type inférieur à 0");
		}
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		if (type==null)
		{
			throw new IllegalArgumentException("erreur le type est vide");
		}
		this.type = type;
	}

	@Override
	public String toString() {
		return "Type_produit [id_type=" + idtype + ", type=" + type + "]";
	}
	
	

}
