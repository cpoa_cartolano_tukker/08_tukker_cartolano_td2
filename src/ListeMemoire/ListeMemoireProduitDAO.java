package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import dao.InterfaceGenericProduitDao;
import metiers.Produit;


public class ListeMemoireProduitDAO implements InterfaceGenericProduitDao {

	private static ListeMemoireProduitDAO instance;

	private List<Produit> donnees;

	public static ListeMemoireProduitDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireProduitDAO();
		}

		return instance;
	}

	private ListeMemoireProduitDAO() {

		this.donnees = new ArrayList<Produit>();

		this.donnees.add(new Produit(1,"Chaise", 40,2));
		this.donnees.add(new Produit(2,"Table",100,5));
	}

	public void create(Produit objet) {

		// TODO voir le cours 0 pour que contains donne le r�sultat escompt� !
		if (this.donnees.contains(objet)) {
			throw new IllegalArgumentException("Tentative d'insertion d'un doublon");
		} else {
			this.donnees.add(objet);
		}
	}

	public void update(Produit objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	public void delete(Produit objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public Produit getById(int id) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(new Produit(1,"test", 24,0));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne poss�de cet identifiant");
		} else {
			return this.donnees.get(idx);
		}
	}

	public List<Produit> findAllProduit() {

		return this.donnees;
	}

	
}

	
