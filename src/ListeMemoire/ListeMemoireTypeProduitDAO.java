package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import dao.InterfaceGenericTypeProduitDao;
import metiers.TypeProduit;

public class ListeMemoireTypeProduitDAO implements InterfaceGenericTypeProduitDao {

	private static ListeMemoireTypeProduitDAO instance;

	private List<TypeProduit> donnees;

	public static ListeMemoireTypeProduitDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireTypeProduitDAO();
		}

		return instance;
	}

	private ListeMemoireTypeProduitDAO() {

		this.donnees = new ArrayList<TypeProduit>();

		this.donnees.add(new TypeProduit(1,"Meuble"));
		this.donnees.add(new TypeProduit(2,"voiture"));
	}

	public void create(TypeProduit objet) {

		// TODO voir le cours 0 pour que contains donne le r�sultat escompt� !
		if (this.donnees.contains(objet)) {
			throw new IllegalArgumentException("Tentative d'insertion d'un doublon");
		} else {
			this.donnees.add(objet);
		}
	}

	public void update(TypeProduit objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	public void delete(TypeProduit objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public TypeProduit getById(int id) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(new TypeProduit(1,"test"));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne poss�de cet identifiant");
		} else {
			return this.donnees.get(idx);
		}
	}

	

	@Override
	public List<TypeProduit> findAllTypeProduit() {
		return this.donnees;
	}

	

}