package ListeMemoire;




import java.util.List;

import dao.InterfaceGenericFactureDao;
import metiers.Facture;


public class ListeMemoireFactureDAO implements InterfaceGenericFactureDao {

	private static ListeMemoireFactureDAO instance;

	private List<Facture> donnees;

	public static ListeMemoireFactureDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireFactureDAO();
		}

		return instance;
	}

	private ListeMemoireFactureDAO() {

		

		this.donnees.add(new Facture(1,1,"11/12/2015"));
		this.donnees.add(new Facture(2,1,"10/12/2000"));
	} 

	public void create(Facture objet) {

		// TODO voir le cours 0 pour que contains donne le résultat escompté !
		if (this.donnees.contains(objet)) {
			throw new IllegalArgumentException("Tentative d'insertion d'un doublon");
		} else {
			this.donnees.add(objet);
		}
	}

	public void update(Facture objet) {

		// TODO voir le cours 0 pour que indexOf donne le résultat escompté !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	public void delete(Facture objet) {

		// TODO voir le cours 0 pour que indexOf donne le résultat escompté !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public Facture getById(int id) {

		//TODO voir le cours 0 pour que indexOf donne le résultat escompté !
		int idx = this.donnees.indexOf(new Facture(1, 4,"10/08/2015"));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");
	} else {
			return this.donnees.get(idx);
		}
	}

	

	@Override
	public List<Facture> findAllFacture() {
		return this.donnees;
	}

	

	

}