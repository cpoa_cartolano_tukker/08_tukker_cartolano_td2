package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import dao.DAOFactory;
import dao.EPersistance;
import dao.InterfaceGenericTvaDao;
import metiers.Tva;

public class ListeMemoireTvaDAO implements InterfaceGenericTvaDao {

	private static ListeMemoireTvaDAO instance;

	private List<Tva> donnees;
	
	private InterfaceGenericTvaDao dao;
	
	

	public static ListeMemoireTvaDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireTvaDAO();
		}

		return instance;
	}

	private ListeMemoireTvaDAO() {

		this.donnees = new ArrayList<Tva>();

		this.donnees.add(new Tva("Normale", 0.2));
		this.donnees.add(new Tva("Réduite", 0.055));
	}

	public void create(Tva objet) {

		// TODO voir le cours 0 pour que contains donne le r�sultat escompt� !
				if (this.donnees.contains(objet)) {
					throw new IllegalArgumentException("Tentative d'insertion d'un doublon");
				} else {
					this.donnees.add(objet);
				}
	}

	public void update(Tva objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	public void delete(Tva objet) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	public Tva getById(int id) {

		// TODO voir le cours 0 pour que indexOf donne le r�sultat escompt� !
		int idx = this.donnees.indexOf(new Tva("test", 0.5));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne poss�de cet identifiant");
		} else {
			return this.donnees.get(idx);
		}
	}

	public List<Tva> findAllTva() {

		return this.donnees;
	}
	
	
	

}